<div id="welcome">
	<div class="row">
		<div class="wlcLeft col-6 fl">
			<h3>WELCOME TO</h3>
			<h1>HAWKEYE CONSTRUCTION SERVICES INC</h1>
			<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse hendrerit ligula quis velit pharetra egestas. Ut molestie elit non massa. Vivamus tincidunt bibendum velit. Quisque nisl diam, nonummy eu, lobortis consequat, mollis ut, tellus. In iaculis. Integer iaculis justo. Duis velit enim, tristique sed, elementum vel, vehicula condimentum, felis. Aenean nisi odio, porta vel, tristique vel, semper mollis, mi.</p>
			<p>Donec gravida, ipsum vitae convallis sodales, urna justo commodo nunc, non venenatis nisl urna non purus. Mauris lectus lorem, rhoncus non, tincidunt ut, laoreet ac, augue. Aenean dignissim diam at risus. Aliquam adipiscing elementum lorem. Aliquam ut erat. Donec eget urna eget est euismod imperdiet.</p>
			<a href="services#content" class="btn">LEARN MORE</a>
		</div>
		<div class="wlcRight col-6 fr">
			<div class="top">
				<p>CA. #1015307 LICENSED & INSURED </p>
			</div>
			<div class="container">
				<dl>
					<dt> <img src="public/images/common/sprite-container.png" alt="Reliability" class="wlc1"> </dt>
					<dd>
						<h4>Reliability</h4>
						<p>We make sure we have all the tools and equipment needed to complete the job in a timely manner.</p>
					</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/common/sprite-container.png" alt="Professionalism" class="wlc2"></dt>
					<dd>
						<h4>Professionalism</h4>
						<p>Our polite, Service Professionals treat your place as if it were their own.</p>
					</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/common/sprite-container.png" alt="Quality" class="wlc3"></dt>
					<dd>
						<h4>Quality</h4>
						<p>Our skills take pride in their work and strive to get the job done right the first time.</p>
					</dd>
				</dl>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="services">
	<div class="row">
		<div class="container">
			<div class="contServices">
				<img src="public/images/content/services1.jpg" alt="Services Image">
				<p class="description">HVAC</p>
			</div>
			<div class="contServices">
				<img src="public/images/content/services2.jpg" alt="Services Image">
				<p class="description">ELECTRICAL</p>
			</div>
			<div class="contServices">
				<img src="public/images/content/services3.jpg" alt="Services Image">
				<p class="description">PLUMBING</p>
			</div>
			<div class="contServices last">
				<h2>Our<span>Services</span></h2>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse hendrerit ligula quis velit pharetra egestas. Ut molestie elit non massa. Vivamus tincidunt bibendum velit. Quisque nisl diam, nonummy eu, lobortis consequat, mollis ut, tellus. In iaculis. Integer iaculis justo. perdiet.</p>
				<a href="services#content" class="btn">LEARN MORE</a>
			</div>
		</div>
	</div>
</div>
<div id="gallery">
	<div class="row">
		<h2>Our Gallery</h2>
	</div>
	<div class="row row2">
		<div class="container1">
			<img src="public/images/content/gallery1.jpg" alt="Gallery 1">
			<img src="public/images/content/gallery2.jpg" alt="Gallery 2">
			<img src="public/images/content/gallery3.jpg" alt="Gallery 3">
			<img src="public/images/content/gallery4.jpg" alt="Gallery 4">
		</div>
		<div class="container2">
			<img src="public/images/content/gallery5.jpg" alt="Gallery 5">
			<img src="public/images/content/gallery6.jpg" alt="Gallery 6">
			<img src="public/images/content/gallery7.jpg" alt="Gallery 7">
			<img src="public/images/content/gallery8.jpg" alt="Gallery 8">
		</div>
	</div>
</div>
<div id="help">
	<div class="row">
		<h2>WE’RE COMMITTED TO HELPING YOU</h2>
		<p> Reliable and professional staff that will help you every step <span> of the way. Contact us for a free consultation Today! </span></p>
		<a href="contact#content" class="btn">LEARN MORE</a>
	</div>
</div>
