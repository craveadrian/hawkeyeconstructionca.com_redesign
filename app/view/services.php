<div id="content">
  <div class="row">
    <h1>Services</h1>
    <div class="container">
      <h2>We provide the following services:</h2>
      <ul>
        <li>Bathroom Remodeling</li>
        <li>Kitchen Remodeling</li>
        <li>Window Repair and Installation</li>
        <li>Door Repair and Installation</li>
        <li>Ceramic Flooring</li>
        <li>Carpentry Repairs</li>
      </ul>
    </div>
  </div>
</div>
